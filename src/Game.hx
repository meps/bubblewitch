package;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;
import openfl.display.GradientType;
import openfl.display.Graphics;
import openfl.display.Sprite;
import openfl.geom.Matrix;

class Game {
	
	public function new(aTarget:Sprite) {
		_target = aTarget;
		initGraphics();
		initField();
		drawField();
		dropField();
		shotAdd();
	}
	
	public function update(aDeltaTime:Int) {
		if (_state != SHOT)
			return;
		
		_shotX += _velocityX;
		_shotY += _velocityY;
		var right = (FIELD_WIDTH * 2 - 1) * RADIUS_VISIBLE;
		if (_shotX <= 0) {
			_shotX = 0;
			_velocityX = -_velocityX;
		} else if (_shotX >= right) {
			_shotX = right;
			_velocityX = -_velocityX;
		}
		_shot.x = _shotX;
		_shot.y = _shotY;
		
		var coords = screenToCoords(_shotX, _shotY);
		var cell = fieldGet(coords);
		
		if (_shotY > 0 && cell == null) {
			// изменить координаты последнего расположения
			if (_shotCoords == null || _shotCoords.row != coords.row || _shotCoords.column != coords.column)
				_shotCoords = coords;
			return;
		}
		
		if (cell != null)
			// поместить шар в предыдущей не занятой точке
			coords = _shotCoords;
		
		ballRemove(_shotColor, _shot);
		fieldSet(coords, _shotColor);
		var count = markFlood(coords, _shotColor);
		if (count >= 3) {
			markRemove(true);
			dropField();
		} else {
			markClear();
		}
		shotAdd();
	}
	
	public function click(aX:Float, aY:Float) {
		if (_state == SHOT)
			// не принимать выстрел, пока не отработан предыдущий
			return;
		
		// новый выстрел
		_state = SHOT;
		var dx = aX - _shotX - RADIUS_VISIBLE;
		var dy = aY - _shotY - RADIUS_VISIBLE;
		if (dy > 0)
			// выстрел только вверх
			dy = -dy;
		var ratio = SHOT_VELOCITY / Math.sqrt(dx * dx + dy * dy);
		_velocityX = dx * ratio;
		_velocityY = dy * ratio;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	// подготовить графику шариков
	function initGraphics() {
		_ballBitmaps = [];
		_ballPool = [];
		var alphas = [1.0, 1.0, 1.0, 1.0];
		var ratios = [0, 90, 220, 255];
		var matrix = new Matrix();
		matrix.createGradientBox(RADIUS_VISIBLE * 3, RADIUS_VISIBLE * 3, 0, -RADIUS_VISIBLE * 0.9, -RADIUS_VISIBLE);
		for (color in COLORS) {
			var colors = [colorTint(color, 1.0), color, colorTint(color, -0.7), colorTint(color, -0.65)];
			var sprite = new Sprite();
			var graph:Graphics = sprite.graphics;
			graph.beginGradientFill(GradientType.RADIAL, colors, alphas, ratios, matrix);
			graph.drawCircle(RADIUS_VISIBLE, RADIUS_VISIBLE, RADIUS_VISIBLE);
			var size = Std.int(RADIUS_VISIBLE * 2);
			var raster = new BitmapData(size, size, true, 0x00000000);
			raster.draw(sprite, null, null, null, null, true);
			_ballBitmaps.push(raster);
		}
	}
	
	// изменение яркости базового цвета
	function colorTint(aColor:UInt, aTint:Float):UInt {
		var r = (aColor >> 16) & 0xFF;
		var g = (aColor >> 8) & 0xFF;
		var b = aColor & 0xFF;
		if (aTint > 0) {
			// высветление
			if (aTint > 1.0)
				aTint = 1.0;
			r = Std.int(r + (255 - r) * aTint);
			g = Std.int(g + (255 - g) * aTint);
			b = Std.int(b + (255 - b) * aTint);
		} else {
			// затемнение
			if (aTint < -1.0)
				aTint = -1.0;
			r = Std.int(r * (1.0 + aTint));
			g = Std.int(g * (1.0 + aTint));
			b = Std.int(b * (1.0 + aTint));
		}
		return (r << 16) | (g << 8) | b;
	}
	
	function initField() {
		_field = [];
		for (row in 0...FIELD_HEIGHT) {
			var line = _field[row] = [];
			var prob = row / FIELD_HEIGHT;
			for (column in 0...FIELD_HEIGHT) {
				if (Math.random() > prob)
					line[column] = { color:Std.int(Math.random() * COLORS.length), mark:false };
				else
					line[column] = null;
			}
		}
	}
	
	function drawField() {
		var y = 0.0;
		for (row in 0...FIELD_HEIGHT) {
			var line = _field[row];
			var x = (row & 1) * RADIUS_VISIBLE;
			for (column in 0...FIELD_WIDTH) {
				var cell = line[column];
				if (cell != null) {
					var ball = ballAdd(cell.color);
					ball.x = x;
					ball.y = y;
					cell.ball = ball;
				}
				x += RADIUS_VISIBLE * 2;
			}
			y += RADIUS_VISIBLE * 1.732;
		}
	}
	
	// сбросить все незакрепленные шарики
	function dropField() {
		var coord = { row:0, column:0 };
		for (column in 0...FIELD_WIDTH) {
			coord.column = column;
			markFlood(coord);
		}
		markRemove(false); // сбросить все не помеченные
	}
	
	// сбросить все пометки
	function markClear() {
		for (row in 0...FIELD_HEIGHT) {
			var line = _field[row];
			for (column in 0...FIELD_WIDTH) {
				var cell = line[column];
				if (cell != null)
					cell.mark = false;
			}
		}
	}
	
	// пометить связный регион
	function markFlood(aSeed:Coords, aColor:Int = -1):Int {
		var count = 0;
		var queue = [aSeed];
		while (queue.length > 0) {
			var coord = queue.pop();
			var row = coord.row;
			var column = coord.column;
			var cell = _field[row][column];
			if (cell == null || cell.mark)
				continue;
			if (aColor >= 0 && cell.color != aColor)
				continue;
			++count;
			cell.mark = true;
			
			// левые и правые соседи
			if (column > 0)
				queue.push( { row:row, column:column - 1 } );
			if (column < FIELD_WIDTH - 1)
				queue.push( { row:row, column:column + 1 } );
			
			var odd = (row & 1) != 0; // нечетные смещенные ряды
			
			// верхние соседи
			if (row > 0) {
				queue.push( { row:row - 1, column:column } );
				if (odd) {
					if (column < FIELD_WIDTH - 1)
						queue.push( { row:row - 1, column:column + 1 } );
				} else {
					if (column > 0)
						queue.push( { row:row - 1, column:column - 1 } );
				}
			}
			
			// нижние соседи
			if (row < FIELD_HEIGHT - 1) {
				queue.push( { row:row + 1, column:column } );
				if (odd) {
					if (column < FIELD_WIDTH - 1)
						queue.push( { row:row + 1, column:column + 1 } );
				} else {
					if (column > 0)
						queue.push( { row:row + 1, column:column - 1 } );
				}
			}
		}
		return count;
	}
	
	// удалить все помеченные шарики и сбросить флаги
	function markRemove(aMark:Bool) {
		for (row in 0...FIELD_HEIGHT) {
			var line = _field[row];
			for (column in 0...FIELD_WIDTH) {
				var cell = line[column];
				if (cell != null) {
					if (cell.mark == aMark) {
						ballRemove(cell.color, cell.ball);
						line[column] = null;
					} else {
						line[column].mark = false;
					}
				}
			}
		}
	}
	
	// перевод экранных координат в индексы поля
	function screenToCoords(aX:Float, aY:Float):Coords {
		var row = Std.int(aY / (RADIUS_VISIBLE * 1.732) + 0.5);
		var column = Std.int((aX - (row & 1) * RADIUS_VISIBLE) / (RADIUS_VISIBLE * 2) + 0.5);
		return { row:row, column:column };
	}
	
	function fieldSet(aCoords:Coords, aColor:Int) {
		var row = aCoords.row;
		var column = aCoords.column;
		if (row < 0 || row >= FIELD_HEIGHT || column < 0 || column >= FIELD_WIDTH)
			return;
		
		var cell = _field[row][column];
		if (cell != null)
			ballRemove(cell.color, cell.ball);
		var ball = ballAdd(aColor);
		ball.x = ((row & 1) + column * 2) * RADIUS_VISIBLE;
		ball.y = row * RADIUS_VISIBLE * 1.732;
		_field[row][column] = { color:aColor, mark:false, ball:ball };
	}
	
	function fieldGet(aCoords:Coords):Cell {
		var row = aCoords.row;
		if (row >= _field.length)
			return null;
		
		var list = _field[row];
		var column = aCoords.column;
		if (column >= list.length)
			return null;
		
		return list[column];
	}
	
	function ballAdd(aColor:Int):DisplayObject {
		var list = _ballPool[aColor];
		var ball = list == null || list.length == 0 ? new Bitmap(_ballBitmaps[aColor]) : list.pop();
		_target.addChild(ball);
		return ball;
	}
	
	function ballRemove(aColor:Int, aBall:DisplayObject) {
		var list = _ballPool[aColor];
		if (list == null)
			list = _ballPool[aColor] = [];
		_target.removeChild(aBall);
		list.push(aBall);
	}
	
	// создать новый шар для выстрела
	function shotAdd() {
		_shotCoords = null;
		_shotColor = Std.int(Math.random() * COLORS.length);
		_shotX = (FIELD_WIDTH - 1) * RADIUS_VISIBLE;
		_shotY = (FIELD_HEIGHT - 1) * 2 * RADIUS_VISIBLE;
		_shot = ballAdd(_shotColor);
		_shot.x = _shotX;
		_shot.y = _shotY;
		_state = WAIT;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var _state:State = WAIT;
	var _target:Sprite;
	var _ballBitmaps:Array<BitmapData>;
	var _ballPool:Array<Array<DisplayObject>>;
	var _field:Array<Array<Cell>>;
	
	var _shot:DisplayObject;
	var _shotCoords:Coords;
	var _shotColor:Int;
	var _shotX:Float;
	var _shotY:Float;
	var _velocityX:Float;
	var _velocityY:Float;
	
	static var COLORS:Array<UInt> = [0xFF0000, 0xFF9900, 0xFFFF00, 0x00FF00, 0x00FFFF, 0x0000FF, 0xFF00FF];
	static inline var RADIUS_VISIBLE = 20.0;
	static inline var SHOT_VELOCITY = 7.0; // пикселей/кадр
	static inline var FIELD_WIDTH = 16;
	static inline var FIELD_HEIGHT = 16;
	
}

typedef Cell = {
	var color:Int;
	var mark:Bool;
	@:optional var ball:DisplayObject;
}

typedef Coords = {
	var row:Int;
	var column:Int;
}

@:enum abstract State(Int) {
	var WAIT = 0; // ожидание выстрела
	var SHOT = 1; // выстрел
}