package;

import Game;
import openfl.Lib;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;

class Main extends Sprite {
	
	public function new() {
		super();
		_game = new Game(this);
		if (stage != null)
			onInit();
		else
			addEventListener(Event.ADDED_TO_STAGE, onInit);
	}
	
	function onInit(?aEvent:Event) {
		if (aEvent != null)
			removeEventListener(Event.ADDED_TO_STAGE, onInit);
		
		stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		stage.addEventListener(MouseEvent.CLICK, onMouseClick);
	}
	
	function onEnterFrame(aEvent:Event) {
		var time = Lib.getTimer();
		_game.update(time - _lastTime);
		_lastTime = time;
	}
	
	function onMouseClick(aEvent:MouseEvent) {
		_game.click(aEvent.stageX, aEvent.stageY);
	}
	
	var _game:Game;
	var _lastTime:Int = 0;
	
}
